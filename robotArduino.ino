/*
Todbot Arduino Package
Created by: The Todbot Team
Version: 0.4
Licence: MIT
Status: Beta
 */


//1.  Load All Dependencies needed (Some may need installation through the Library Manager)

 
#include <DualVNH5019MotorShield.h>
#include <ros.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <Servo.h>
#include <Encoder.h>

//2.  Initialization of dependencies and configuration
unsigned long currentTimeR;
unsigned long currentTimeL;

long oldTimeR = 0;
long oldTimeL = 0;

float speedR = 0;
float speedL = 0;

long oldPositionR  = -999;
long oldPositionL  = -999;

float distance = 0.010687025;

int m1State = 0;
int m2State = 0;

int angle = 180;

long period = 10000;
unsigned long time_now = 0;

ros::NodeHandle nh;

Servo servo;
DualVNH5019MotorShield md;

Encoder R(20, 21);
Encoder L(22, 23);

//3.  Motor Configurarion

void motor_sCb( const std_msgs::Int32& input){
  switch(input.data)
  {
    case 1:
      m1State = -100;
      m2State = -100;
      break;
    case 2:
      m1State = 100;
      m2State = 100;
      break;
    case 3:
      m1State = -100;
      m2State = 100;
      break;
    case 4:
      m1State = 100;
      m2State = -100;
      break;
    case 5:
      m1State = 0;
      m2State = 0;
      break;
  }
  
}


void motorR_fCb( const std_msgs::Int32& input){
  m1State = input.data;
}

void motorL_fCb( const std_msgs::Int32& input){
  m2State = input.data;
}


//4.  Servo Configurations

void servo_sCb( const std_msgs::Int32& input){
  switch(input.data)
  {
    case 2:
      angle = 90;
      break;
    case 1:
      angle = 180;
      break;
    case 3: 
      angle = 10;
      break;
  }
}

void servo_fCb( const std_msgs::Int32& input){
  angle = input.data;
}


//5.  ROS Node Initialization

ros::Subscriber<std_msgs::Int32> motor_sSUB("motor_simple", &motor_sCb );
ros::Subscriber<std_msgs::Int32> motorL_fSUB("motorL_fine", &motorL_fCb );
ros::Subscriber<std_msgs::Int32> motorR_fSUB("motorR_fine", &motorR_fCb );
ros::Subscriber<std_msgs::Int32> servo_sSUB("servo_simple", &servo_sCb );
ros::Subscriber<std_msgs::Int32> servo_fSUB("servo_fine", &servo_fCb );
std_msgs::Float32 motorR_vCb;
std_msgs::Float32 motorL_vCb;
ros::Publisher  motorR_vPUB("motorR_vel", &motorR_vCb);
ros::Publisher  motorL_vPUB("motorL_vel", &motorR_vCb);


void setup() { 
  // Initialize Motor Shield
  md.init();

  // Subscribe to alll nodes used
  nh.initNode();
  nh.subscribe(motor_sSUB);
  nh.subscribe(motorL_fSUB);
  nh.subscribe(motorR_fSUB);
  nh.subscribe(servo_sSUB);
  nh.subscribe(servo_fSUB);
  nh.advertise(motorR_vPUB);
  nh.advertise(motorL_vPUB);
  // Attach to Servo and make boot sequence
  servo.attach(11);
  servo.write(0);
  delay(1000);
  servo.write (180);
  delay (1000);
  servo.write(0);
  delay(1000);
  servo.write (180);
}

void loop() {
  time_now = micros();
  currentTimeR = micros ();
  long newPositionR = R.read();
  long newPositionL = L.read();
  if (newPositionR != oldPositionR) {
    speedR = distance * (newPositionR - oldPositionR) / (currentTimeR - oldTimeR);
    speedR = speedR *10000;
    oldPositionR = newPositionR;
    oldTimeR = currentTimeR;
  } 
  else {
    speedR = 0;
    }
  currentTimeL = micros ();
  if (newPositionL != oldPositionL) {
    speedL = distance * (newPositionL - oldPositionL) / (currentTimeL - oldTimeL);
    speedL = speedL *10000;
    oldPositionL = newPositionL;
    oldTimeL = currentTimeL;
  } 
  else {
    speedL = 0;
    }
  motorR_vCb.data = speedR;
  motorR_vPUB.publish(&motorR_vCb);
  motorL_vCb.data = speedL;
  motorL_vPUB.publish(&motorL_vCb);
  //motor 1  is right
  //motor 2 is left
  md.setM2Speed(m2State);
  md.setM1Speed(m1State);

  servo.write(angle);

  while(micros() < time_now + period){
      //wait approx. [period] ms
  }
  
 nh.spinOnce();
}